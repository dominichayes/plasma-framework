# Translation of libplasma5.po to Brazilian Portuguese
# Copyright (C) 2013-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2013, 2014, 2015, 2016, 2018, 2019.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2018, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: libplasma5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-22 01:45+0000\n"
"PO-Revision-Date: 2023-07-04 10:50-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:81
#, kde-format
msgid "More actions"
msgstr "Mais ações"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:568
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr "Recolher"

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:568
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr "Expandir"

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:46
#, kde-format
msgid "Password"
msgstr "Senha"

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:60
#, kde-format
msgid "Search…"
msgstr "Pesquisar..."

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:62
#, kde-format
msgid "Search"
msgstr "Pesquisar"

#: plasma/applet.cpp:302
#, kde-format
msgid "Unknown"
msgstr "Desconhecido"

#: plasma/applet.cpp:745
#, kde-format
msgid "Activate %1 Widget"
msgstr "Ativar o widget %1"

#: plasma/containment.cpp:96 plasma/private/applet_p.cpp:108
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Remover %1"

#: plasma/containment.cpp:102 plasma/containment.cpp:113 plasma/corona.cpp:367
#: plasma/corona.cpp:485
#, kde-format
msgid "Enter Edit Mode"
msgstr "Entrar no modo de edição"

#: plasma/containment.cpp:105 plasma/private/applet_p.cpp:113
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Configurar %1..."

#: plasma/containment.cpp:111 plasma/corona.cpp:365
#, kde-format
msgid "Exit Edit Mode"
msgstr "Sair do modo de edição"

#: plasma/corona.cpp:316 plasma/corona.cpp:470
#, kde-format
msgid "Lock Widgets"
msgstr "Bloquear widgets"

#: plasma/corona.cpp:316
#, kde-format
msgid "Unlock Widgets"
msgstr "Desbloquear widgets"

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Se deve criar ou não um cache no disco para o tema."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"O tamanho máximo do cache do tema no disco em quilobytes. Lembre-se de que "
"esses arquivos são esparsos, sendo que o tamanho máximo poderá não ser "
"usado. A atribuição de um tamanho mais elevado normalmente é bastante seguro."

#: plasma/private/applet_p.cpp:128
#, kde-format
msgid "The %1 widget did not define which ScriptEngine to use."
msgstr "O widget %1 não define qual ScriptEngine usar."

#: plasma/private/applet_p.cpp:146
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr "Não foi possível abrir o pacote %1, necessário para o widget %2."

#: plasma/private/applet_p.cpp:154
#, kde-format
msgid "Show Alternatives..."
msgstr "Mostrar alternativas..."

#: plasma/private/applet_p.cpp:262
#, kde-format
msgid "Widget Removed"
msgstr "Widget removido"

#: plasma/private/applet_p.cpp:263
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "O widget \"%1\" foi removido."

#: plasma/private/applet_p.cpp:267
#, kde-format
msgid "Panel Removed"
msgstr "Painel removido"

#: plasma/private/applet_p.cpp:268
#, kde-format
msgid "A panel has been removed."
msgstr "Um painel foi removido."

#: plasma/private/applet_p.cpp:271
#, kde-format
msgid "Desktop Removed"
msgstr "Área de trabalho removida"

#: plasma/private/applet_p.cpp:272
#, kde-format
msgid "A desktop has been removed."
msgstr "Uma área de trabalho foi removida."

#: plasma/private/applet_p.cpp:275
#, kde-format
msgid "Undo"
msgstr "Desfazer"

#: plasma/private/applet_p.cpp:367
#, kde-format
msgid "Widget Settings"
msgstr "Configurações do widget"

#: plasma/private/applet_p.cpp:374
#, kde-format
msgid "Remove this Widget"
msgstr "Remover este widget"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Remover este painel"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Remover esta atividade"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Configurações da atividade"

#: plasma/private/containment_p.cpp:78
#, kde-format
msgid "Add Widgets..."
msgstr "Adicionar widgets..."

#: plasma/private/containment_p.cpp:197
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Não foi possível encontrar o componente requisitado: %1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"O compartilhamento de um widget na rede permite-lhe acessá-lo a partir de "
"outro computador, como se fosse um controle remoto."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Compartilhar este widget na rede"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr "Permitir que todos tenham livre acesso a este widget"

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Serviço inválido (null), não pode executar quaisquer operações."

#: plasmaquick/appletquickitem.cpp:529
#, kde-format
msgid "The root item of %1 must be of type ContaimentItem"
msgstr "O item raiz de %1 deve ser do tipo ContaimentItem"

#: plasmaquick/appletquickitem.cpp:534
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr "O item raiz de %1 deve ser do tipo PlasmoidItem"

#: plasmaquick/appletquickitem.cpp:541
#, kde-format
msgid "Unknown Applet"
msgstr "Miniaplicativo desconhecido"

#: plasmaquick/appletquickitem.cpp:557
#, kde-format
msgid "Error loading QML file: %1 %2"
msgstr "Ocorreu um erro ao carregar o arquivo QML: %1 %2"

#: plasmaquick/appletquickitem.cpp:559
#, kde-format
msgid "Error loading Applet: package inexistent. %1"
msgstr "Erro ao carregar o miniaplicativo: O pacote não existe. %1"

#: plasmaquick/configview.cpp:101
#, kde-format
msgid "%1 Settings"
msgstr "Configurações de %1"

#: plasmaquick/plasmoid/containmentitem.cpp:553
#, kde-format
msgid "Plasma Package"
msgstr "Pacotes do Plasma"

#: plasmaquick/plasmoid/containmentitem.cpp:557
#, kde-format
msgid "Install"
msgstr "Instalar"

#: plasmaquick/plasmoid/containmentitem.cpp:568
#, kde-format
msgid "Package Installation Failed"
msgstr "Ocorreu um erro na instalação do pacote"

#: plasmaquick/plasmoid/containmentitem.cpp:584
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "O pacote que acabou de soltar é inválido."

#: plasmaquick/plasmoid/containmentitem.cpp:593
#: plasmaquick/plasmoid/containmentitem.cpp:662
#, kde-format
msgid "Widgets"
msgstr "Widgets"

#: plasmaquick/plasmoid/containmentitem.cpp:598
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Adicionar %1"

#: plasmaquick/plasmoid/containmentitem.cpp:612
#: plasmaquick/plasmoid/containmentitem.cpp:666
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Adicionar ícone"

#: plasmaquick/plasmoid/containmentitem.cpp:624
#, kde-format
msgid "Wallpaper"
msgstr "Papel de parede"

#: plasmaquick/plasmoid/containmentitem.cpp:634
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Configurar %1"

#: plasmaquick/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Conteúdo solto"

#~ msgctxt "misc category"
#~ msgid "Miscellaneous"
#~ msgstr "Diversos"

#~ msgid "Main Script File"
#~ msgstr "Arquivo principal do script"

#~ msgid "Tests"
#~ msgstr "Testes"

#~ msgid "Images"
#~ msgstr "Imagens"

#~ msgid "Themed Images"
#~ msgstr "Imagens com tema"

#~ msgid "Configuration Definitions"
#~ msgstr "Definições de configuração"

#~ msgid "User Interface"
#~ msgstr "Interface do usuário"

#~ msgid "Data Files"
#~ msgstr "Arquivos de dados"

#~ msgid "Executable Scripts"
#~ msgstr "Scripts executáveis"

#~ msgid "Screenshot"
#~ msgstr "Imagem"

#~ msgid "Translations"
#~ msgstr "Traduções"

#~ msgid "Configuration UI pages model"
#~ msgstr "Modelo de página UI de configuração"

#~ msgid "Configuration XML file"
#~ msgstr "Arquivo de configuração XML"

#~ msgid "Custom expander for compact applets"
#~ msgstr "Expansão personalizada para miniaplicativos compactos"

#~ msgid "Images for dialogs"
#~ msgstr "Imagens para os diálogos"

#~ msgid "Generic dialog background"
#~ msgstr "Fundo genérico do diálogo"

#~ msgid "Theme for the logout dialog"
#~ msgstr "Tema para o diálogo de encerramento"

#~ msgid "Wallpaper packages"
#~ msgstr "Pacotes de papel de parede"

#~ msgid "Images for widgets"
#~ msgstr "Imagens para os widgets"

#~ msgid "Background image for widgets"
#~ msgstr "Imagem de fundo dos widgets"

#~ msgid "Analog clock face"
#~ msgstr "Face do relógio analógico"

#~ msgid "Background image for panels"
#~ msgstr "Imagem de fundo dos painéis"

#~ msgid "Background for graphing widgets"
#~ msgstr "Fundo dos elementos gráficos"

#~ msgid "Background image for tooltips"
#~ msgstr "Imagem de fundo das dicas"

#~ msgid "Opaque images for dialogs"
#~ msgstr "Imagens opacas para os diálogos"

#~ msgid "Opaque generic dialog background"
#~ msgstr "Fundo opaco genérico do diálogo"

#~ msgid "Opaque theme for the logout dialog"
#~ msgstr "Tema opaco para o diálogo de encerramento"

#~ msgid "Opaque images for widgets"
#~ msgstr "Imagens opacas para os widgets"

#~ msgid "Opaque background image for panels"
#~ msgstr "Imagem de fundo opaca para os painéis"

#~ msgid "Opaque background image for tooltips"
#~ msgstr "Imagem de fundo opaca para as dicas"

#~ msgid "KColorScheme configuration file"
#~ msgstr "Arquivo de configuração do KColorScheme"

#~ msgid "Service Descriptions"
#~ msgstr "Descrições dos serviços"

#~ msgctxt ""
#~ "API or programming language the widget was written in, name of the widget"
#~ msgid "Could not create a %1 ScriptEngine for the %2 widget."
#~ msgstr "Não foi possível criar uma ScriptEngine %1, para o widget %2."

#~ msgid "Script initialization failed"
#~ msgstr "A inicialização do script falhou"

#~ msgctxt "Agenda listview section title"
#~ msgid "Holidays"
#~ msgstr "Feriados"

#~ msgctxt "Agenda listview section title"
#~ msgid "Events"
#~ msgstr "Eventos"

# Deve ficar no plural, porque trata-se de um título que engloba todas as tarefas de um determinado dia. (Alvarenga)
#~ msgctxt "Agenda listview section title"
#~ msgid "Todo"
#~ msgstr "Tarefas"

# Deve ficar no plural, porque trata-se de um título que engloba todos os outros itens de calendário de um determinado dia. (Alvarenga)
#~ msgctxt "Means 'Other calendar items'"
#~ msgid "Other"
#~ msgstr "Outros"

#~ msgctxt "Format: month year"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#~ msgid "Previous Month"
#~ msgstr "Mês anterior"

#~ msgid "Previous Year"
#~ msgstr "Ano anterior"

#~ msgid "Previous Decade"
#~ msgstr "Década anterior"

#~ msgctxt "Reset calendar to today"
#~ msgid "Today"
#~ msgstr "Hoje"

#~ msgid "Reset calendar to today"
#~ msgstr "Redefine o calendário para hoje"

#~ msgid "Next Month"
#~ msgstr "Próximo mês"

#~ msgid "Next Year"
#~ msgstr "Próximo ano"

#~ msgid "Next Decade"
#~ msgstr "Próxima década"

#~ msgid "Days"
#~ msgstr "Dias"

#~ msgid "Months"
#~ msgstr "Meses"

#~ msgid "Years"
#~ msgstr "Anos"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Run the Associated Application"
#~ msgstr "Executar o aplicativo associado"

#~ msgid "Open with %1"
#~ msgstr "Abrir com %1"

#~ msgid "Accessibility"
#~ msgstr "Acessibilidade"

# Nome de categoria. (Alvarenga)
#~ msgid "Application Launchers"
#~ msgstr "Lançadores de Aplicativos"

#~ msgid "Astronomy"
#~ msgstr "Astronomia"

# Nome de categoria. (Alvarenga)
#~ msgid "Date and Time"
#~ msgstr "Data e Hora"

# Nome de categoria. (Alvarenga)
#~ msgid "Development Tools"
#~ msgstr "Ferramentas de Desenvolvimento"

#~ msgid "Education"
#~ msgstr "Educação"

# Nome de categoria. (Alvarenga)
#~ msgid "Environment and Weather"
#~ msgstr "Ambiente e Tempo"

#~ msgid "Examples"
#~ msgstr "Exemplos"

# Nome de categoria. (Alvarenga)
#~ msgid "File System"
#~ msgstr "Sistema de Arquivos"

# Nome de categoria. (Alvarenga)
#~ msgid "Fun and Games"
#~ msgstr "Jogos e Diversões"

# Nome de categoria. (Alvarenga)
#~ msgid "Graphics"
#~ msgstr "Gráficos"

# Nome de categoria. (Alvarenga)
#~ msgid "Language"
#~ msgstr "Idioma"

#~ msgid "Mapping"
#~ msgstr "Mapeamento"

#~ msgid "Miscellaneous"
#~ msgstr "Diversos"

# Nome de categoria. (Alvarenga)
#~ msgid "Multimedia"
#~ msgstr "Multimídia"

# Nome de categoria. (Alvarenga)
#~ msgid "Online Services"
#~ msgstr "Serviços Online"

#~ msgid "Productivity"
#~ msgstr "Produtividade"

# Nome de categoria. (Alvarenga)
#~ msgid "System Information"
#~ msgstr "Informações do Sistema"

# Nome de categoria. (Alvarenga)
#~ msgid "Utilities"
#~ msgstr "Utilitários"

# Nome de categoria. (Alvarenga)
#~ msgid "Windows and Tasks"
#~ msgstr "Janelas e Tarefas"

# Nome de categoria. (Alvarenga)
#~ msgid "Clipboard"
#~ msgstr "Área de Transferência"

# Nome de categoria. (Alvarenga)
#~ msgid "Tasks"
#~ msgstr "Tarefas"

#~ msgctxt "%1 is the name of the containment"
#~ msgid "Edit %1..."
#~ msgstr "Editar %1..."

#~ msgid "Default settings for theme, etc."
#~ msgstr "Configuração padrão do tema, etc."

#~ msgid "Color scheme to use for applications."
#~ msgstr "Esquema de cores para usar nos aplicativos."

#~ msgid "Preview Images"
#~ msgstr "Visualização de imagens"

#~ msgid "Preview for the Login Manager"
#~ msgstr "Visualização do gerenciador de autenticação"

#~ msgid "Preview for the Lock Screen"
#~ msgstr "Visualização do bloqueio de tela"

#~ msgid "Preview for the Userswitcher"
#~ msgstr "Visualização do seletor de usuários"

#~ msgid "Preview for the Virtual Desktop Switcher"
#~ msgstr "Visualização do alternador de áreas de trabalho virtuais"

#~ msgid "Preview for Splash Screen"
#~ msgstr "Visualização da tela de apresentação"

#~ msgid "Preview for KRunner"
#~ msgstr "Visualização do KRunner"

#~ msgid "Preview for the Window Decorations"
#~ msgstr "Visualização das decorações das janelas"

#~ msgid "Preview for Window Switcher"
#~ msgstr "Visualização do seletor de janelas"

#~ msgid "Login Manager"
#~ msgstr "Gerenciador de autenticação"

#~ msgid "Main Script for Login Manager"
#~ msgstr "Script principal do gerenciador de autenticação"

#~ msgid "Logout Dialog"
#~ msgstr "Caixa de diálogo de desligamento"

#~ msgid "Main Script for Logout Dialog"
#~ msgstr "Script principal da caixa de diálogo de desligamento"

#~ msgid "Screenlocker"
#~ msgstr "Bloqueio de tela"

#~ msgid "Main Script for Lock Screen"
#~ msgstr "Script principal do bloqueio de tela"

#~ msgid "UI for fast user switching"
#~ msgstr "Interface de mudança rápida de usuário"

#~ msgid "Main Script for User Switcher"
#~ msgstr "Script principal do seletor de usuários"

#~ msgid "Virtual Desktop Switcher"
#~ msgstr "Alternador de áreas de trabalho virtuais"

#~ msgid "Main Script for Virtual Desktop Switcher"
#~ msgstr "Script principal do alternador de áreas de trabalho virtuais"

#~ msgid "On-Screen Display Notifications"
#~ msgstr "Notificações visíveis na tela"

#~ msgid "Main Script for On-Screen Display Notifications"
#~ msgstr "Script principal das notificações visíveis na tela"

#~ msgid "Splash Screen"
#~ msgstr "Tela de apresentação"

#~ msgid "Main Script for Splash Screen"
#~ msgstr "Script principal da tela de apresentação"

#~ msgid "KRunner UI"
#~ msgstr "Interface do KRunner"

#~ msgid "Main Script KRunner"
#~ msgstr "Script principal do KRunner"

#~ msgid "Window Decoration"
#~ msgstr "Decorações da janela"

#~ msgid "Main Script for Window Decoration"
#~ msgstr "Script principal da decoração de janelas"

#~ msgid "Window Switcher"
#~ msgstr "Seletor de janelas"

#~ msgid "Main Script for Window Switcher"
#~ msgstr "Script principal do seletor de janelas"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Concluir a personalização do layout"

#~ msgid "Customize Layout..."
#~ msgstr "Personalizar layout..."

#~ msgid "Fetching file type..."
#~ msgstr "Obtendo o tipo de arquivo..."
